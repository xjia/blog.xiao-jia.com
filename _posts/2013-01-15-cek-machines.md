title: 用 Haskell 编写 CEK 风格的解释器
tag: haskell
time: 2013-01-15 13:47:28

这篇文章基本翻译自 Matt Might 的 [Writing CEK-style interpreters (or semantics) in Haskell](http://matt.might.net/articles/cek-machines/)，略去了我认为无关紧要的部分，在不容易理解的地方加入了新的内容。

CEK 状态机（CEK machine）是由 [Matthias Felleisen](http://www.ccs.neu.edu/home/matthias/) 和 [Dan Friedman](https://www.cs.indiana.edu/~dfried/) 设计的一种 λ 演算的机械模型（mechanical model），适用于设计和实现解释器。除了高效之外，CEK 风格的解释器还具有如下特征：

- 易于添加复杂的语言特性，比如 continuation
- 易于中止解释过程，并得到 stack trace
- 易于引入对线程的支持
- 易于在调试器中单步执行

这篇文章使用 Haskell 语言来介绍 CEK 状态机。读者需要熟悉 λ 演算，并理解它是一个不失一般性的编程语言。否则请先阅读下面几篇文章，或者阅读 Benjamin Pierce 的 [Types and Programming Languages](http://www.amazon.com/gp/product/0262162091/ref=as_li_ss_tl?ie=UTF8&tag=ucmbread-20&linkCode=as2&camp=217145&creative=399369&creativeASIN=0262162091) 一书。

- [Implementing Java as a CESK machine, in Java](http://matt.might.net/articles/oo-cesk/)
- [Writing an interpreter, CESK-style](http://matt.might.net/articles/cesk-machines/)
- [Church encodings in JavaScript](http://matt.might.net/articles/js-church/)
- [Compiling up to the λ-calculus](http://matt.might.net/articles/compiling-up-to-lambda-calculus/)
- [7 lines of code, 3 minutes: Implement a programming language](http://matt.might.net/articles/implementing-a-programming-language/)
- [Lambda-calculus in C++ templates](http://matt.might.net/articles/c++-template-meta-programming-with-lambda-calculus/)
- [Church encodings in Scheme](http://matt.might.net/articles/church-encodings-demo-in-scheme/)
- [Memoizing recursive functions in Javascript with the Y combinator](http://matt.might.net/articles/implementation-of-recursive-fixed-point-y-combinator-in-javascript-for-memoization/)

首先，我们需要如下的数据类型，来定义 λ 演算的抽象语法树。

    type Var = String
    data Lambda = Var :=> Exp
    data Exp = Ref Var
             | Lam Lambda
             | Exp :@ Exp

下表给出了上述数据类型和 λ 演算的对应关系：

    λ 演算   Exp 类型
    v        Ref "v"
    λv.e     Lam ("v" :=> e)
    f(e)     f :@ e

注意，这里使用了以冒号开头的类型构造函数（`:=>` 和 `:@`），详情可以参考[这里](http://www.haskell.org/onlinereport/lexemes.html#sect2.4)和[这里](http://www.haskell.org/ghc/docs/7.6.1/html/users_guide/data-type-extensions.html)。此外，本文使用 Haskell 语言的风格，和一般惯用的方式略有不同，本文更倾向于使用贴近数学表示的形式。

-----

在实现 CEK 状态机之前，我们先来回顾一下状态机是什么，以及如何用状态机来描述一个解释器。

从数学上说，一个状态机包括一个状态空间和一个转移关系，这个转移关系确定了一个状态的后续状态。在代码中，状态空间表示为一个类型，而这个类型的值就是一个状态。我们使用 Σ 作为状态空间的类型名。转移关系在最简单的情况下是确定性的，也就是说，一个状态只有唯一的后续状态。在这一条件下，我们可以用一个全函数来描述这个转移关系。我们称这样一个函数为 `step`：

    step :: Σ -> Σ

为了运行一个确定性状态机，我们需要从一个状态转移到另一个状态，直至达到终止状态。函数 `terminal` 描述了这一过程：

    terminal :: (Σ -> Σ) -> (Σ -> Bool) -> Σ -> Σ
    terminal step isFinal ς0 | isFinal ς0 = ς0
                             | otherwise  = terminal step isFinal (step(ς0))

为了使用该函数，我们还需要提供一个 `isFinal` 函数，来判断当前状态是否有后续状态。其类型如下：

    isFinal :: Σ -> Bool

一个状态机的结构就是这样了。为了使用状态机来描述一个解释器，我们需要一个 `inject` 函数，把一个程序映射到一个状态：

    inject :: Program -> Σ

对于 λ 演算来说，`type Program = Exp`。

有了这些函数之后，就很容易定义解释器的求值函数了：

    evaluate :: Program -> Σ
    evaluate pr = terminal step isFinal (inject(pr))

求值函数将一个程序映射到它的终止状态；如果这个程序不终止，这个函数的计算过程也不终止。

-----

下面来说 CEK 状态机。CEK 状态机由三部分组成：控制部分（control）、环境部分（environment）、continuation 部分。一般在程序中命名 continuation 的时候，都会使用 `Kontinuation` 来避免命名冲突（就像 `count` 和 `kount`），所以我猜测 CEK 这个名字就是这样来的（Control, Environment, Kontinuation）。

控制部分就是当前正在求值的表达式。环境部分是一个从变量到值的映射。环境是当前表达式的局部求值上下文。continuation 部分是当前的栈空间，保存了之前的调用帧（活动记录）。continuation 是当前表达式的动态求值上下文。每一帧都表示了一个等待求值的上下文。

在 Haskell 中，状态空间 Σ 表示为一个三元组：

    type Σ    = (Exp,Env,Kont)
    data D    = Clo (Lambda, Env)
    type Env  = Var -> D
    data Kont = Mt
              | Ar (Exp,Env,Kont)
              | Fn (Lambda,Env,Kont)

类型 `D` 包括了所有的值。对于基本的 λ 演算来说，只有一种值，那就是闭包（closure）。一个闭包包括一个 λ 项和一个环境，该环境定义了该 λ 项中自由变元的值。如果我们要在语言中增加其它类型比如整数或字符串，就应该扩充类型 `D`。（`D` 这个名字来源于程序语言的历史，它同时表示“domain of values”和“denotable values”。）

有三种类型的 continuation：空的 continuation（`Mt`），正在等待求值的参数表达式（`Ar`），以及正在对参数求值的函数（`Fn`）。

为了使用这个 CEK 状态机，首先需要将表达式映射到状态：

    inject :: Exp -> Σ
    inject (e) = (e, ρ0, Mt)
     where ρ0 :: Env
           ρ0 = \ x -> error $ "no binding for " ++ x

`step` 函数向前执行一步操作：

    step :: Σ -> Σ
    
    step (Ref x, ρ, κ)                    
       = (Lam lam,ρ',κ) where Clo (lam, ρ') = ρ(x)
    
    step (f :@ e, ρ, κ)                  
       = (f, ρ,  Ar(e, ρ, κ))
    
    step (Lam lam, ρ, Ar(e, ρ', κ))      
       = (e, ρ', Fn(lam, ρ, κ))
    
    step (Lam lam, ρ, Fn(x :=> e, ρ', κ)) 
       = (e, ρ' // [x ==> Clo (lam, ρ)], κ)

下面是刚才用到的两个辅助函数：

    (==>) :: a -> b -> (a,b)
    (==>) x y = (x,y)
    
    (//) :: Eq a => (a -> b) -> [(a,b)] -> (a -> b)
    (//) f [(x,y)] = \ x' ->
                     if (x == x') 
                     then y
                     else f(x')

非形式化地，`step` 函数一共有四种情况：

- 如果正在求值一个变元的引用，直接在环境中查找即可
- 如果正在求值一个函数应用，首先求值函数本身
- 如果函数已经求值完毕，继续对参数求值
- 如果参数已经求值完毕，将函数应用到参数进行求值

终止状态的判断很简单，只要检查是不是一个空的 continuation 就行了：

    isFinal :: Σ -> Bool
    isFinal (Lam _, ρ, Mt) = True
    isFinal _              = False

至此，一个 CEK 状态机就实现好了。

