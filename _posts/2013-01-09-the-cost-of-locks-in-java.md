title: The cost of locks
tag: performance, java, lmax, disruptor
time: 2013-01-09 12:08:17

昨天看了[一篇文章](http://martinfowler.com/articles/lmax.html)，说 [LMAX Exchange](http://www.lmax.com/) 可以做到每秒钟处理六百万条交易，其架构的一部分是一个叫做 [Disruptor](http://lmax-exchange.github.com/disruptor/) 的东西。于是去看了 Disruptor 的[一篇报告](http://disruptor.googlecode.com/files/Disruptor-1.0.pdf)，其中有一个 section 是讲 the cost of locks，觉得十分新鲜，自己试验了一下，代码和实验结果都放在 [gist](https://gist.github.com/4490196) 里了。

<https://gist.github.com/4490196>

