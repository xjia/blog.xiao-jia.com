title: How to run Kilim benchmarks
tag: kilim, erlang, java
time: 2012-12-25 14:30:49

最近在尝试 [Kilim](http://www.malhar.net/sriram/kilim/)，想比较一下它和 Erlang 的性能。Kilim 的代码里就有一个 `bench` 目录，里面就是各种语言的 benchmarks。我只比较了 `BigPingPong` 这一个测试。

Erlang 的 `bigpingpong.erl` 里是有语法错误的，最后的地方应该是下面这样的

    ping ->
        % wait for n-1 msgs
        recv(MainPid, OrigN-1)
      end.

Kilim 的代码中，这里的注释符号 `%` 误写成 `$` 了。

然后是 Kilim 本身。编译可以直接用 `ant`，但是 `ant test` 的时候会抛异常：

    testwoven:
         [echo] Testing Tasks ======================
         [java] .E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E.E
         [java] Time: 0.013
         [java] There were 32 errors:
         [java] 1) testLoop(kilim.test.TestYield)java.lang.VerifyError: Expecting a stackmap frame at branch target 68 in method kilim.Task.invoke(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;Lkilim/Fiber;)Ljava/lang/Object; at offset 16
         [java]     at kilim.test.TestYield.testLoop(TestYield.java:51)
         [java]     at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
         [java]     at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)
         [java]     at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)

其原因好像是 Sun 被 Oracle 收购之后，Java 虚拟机的运行时类型检查被修改了，只要在运行 JVM 的时候加一个额外的参数就可以了。所以修改 `build.xml` 相应部分如下：

    <target name="testwoven">
      <echo message="Testing Tasks ======================" />
      <java classname="junit.textui.TestRunner" fork="yes">
        <classpath refid="kilim.testwovenclasspath"/>
        <assertions>
          <enable/>
        </assertions>
        <arg value="kilim.test.AllWoven" />
        <jvmarg value="-XX:-UseSplitVerifier" />
      </java>
    </target>

注意我们加了一行 `<jvmarg value="-XX:-UseSplitVerifier" />`。

Kilim 的代码编译好之后，`.class` 文件会存在 `classes` 目录里。Benchmark 的代码在 `bench/kilim/bench/BigPingPong.java`，可以按如下步骤编译它：

    $ javac -cp classes/ -d classes/ bench/kilim/bench/BigPingPong.java
    $ java -cp classes/:libs/asm-all-2.2.3.jar kilim.tools.Weaver -d classes/ classes/kilim/bench/
    Wrote: classes//kilim/bench/BigPingPong.class

注意，Kilim 的文档里说，可以 weave 单独的一个类，比如 `java kilim.tools.Weaver -d classes/ kilim.bench.BigPingPong`。但是我试了一下，不好用，可能是因为 `BigPingPong` 还有一个内部类 `BigPingPong$Msg`。

weave 好了之后就可以运行了。注意运行的时候也要加上 `-XX:-UseSplitVerifier` 参数，不然也会抛异常：

    $ java -cp classes/ kilim.bench.BigPingPong 
    Exception in thread "main" java.lang.VerifyError: Expecting a stackmap frame at branch target 68 in method kilim.Task.invoke(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;Lkilim/Fiber;)Ljava/lang/Object; at offset 16
            at java.lang.Class.getDeclaredMethods0(Native Method)
            at java.lang.Class.privateGetDeclaredMethods(Class.java:2442)
            at java.lang.Class.getMethod0(Class.java:2685)
            at java.lang.Class.getMethod(Class.java:1620)
            at sun.launcher.LauncherHelper.getMainMethod(LauncherHelper.java:492)
            at sun.launcher.LauncherHelper.checkAndLoadMain(LauncherHelper.java:484)
    $ java -XX:-UseSplitVerifier -cp classes/ kilim.bench.BigPingPong 
    nTasks : 10, nSchedulers: 1, nThreadsPerScheduler: 1
    Elapsed ms (10 tasks, 100 messages) 13
    Elapsed ms (10 tasks, 100 messages) 1
    Elapsed ms (10 tasks, 100 messages) 1
    Elapsed ms (10 tasks, 100 messages) 1
    Elapsed ms (10 tasks, 100 messages) 1
    Elapsed ms (10 tasks, 100 messages) 1
    Elapsed ms (10 tasks, 100 messages) 1
    Elapsed ms (10 tasks, 100 messages) 1
    Elapsed ms (10 tasks, 100 messages) 1
    Elapsed ms (10 tasks, 100 messages) 1
    Elapsed ms (10 tasks, 100 messages) 1
    Elapsed ms (10 tasks, 100 messages) 1
    Elapsed ms (10 tasks, 100 messages) 1

文末分享一个很好用的 vim 技巧——列编辑。比如 markdown 里面要插入代码段的时候，要在代码段每一行的最开始放四个空格，列编辑就很有用。

- Press <kbd>Ctrl</kbd><kbd>V</kbd> to enter column mode
- Select the columns and rows where you want to enter your text
- <kbd>Shift</kbd><kbd>i</kbd> to enter insert mode in column mode
- Type in the text you want to enter
- Do not be discouraged by the fact that only the first row is changed
- <kbd>Esc</kbd> to apply your change (or alternately <kbd>Ctrl</kbd><kbd>c</kbd>)

