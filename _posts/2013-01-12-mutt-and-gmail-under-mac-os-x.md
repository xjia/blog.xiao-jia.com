title: Mutt and Gmail under Mac OS X
tag: mutt, gmail, mac, osx
time: 2013-01-12 09:51:22

I'm using Mac OS X 10.7.5 but the process should be the same across different versions. 

First, install required tools according to <https://help.ubuntu.com/community/MuttAndGmail>. 
Mac OS X comes with `fetchmail` and `procmail` so we only need to

    brew install msmtp mutt

and create corresponding configuration files under `$HOME`.

There are two things different between Ubuntu and OS X. 

1. `sslcertpath` should be set to `/Users/[your name]/ssl/certs` and manually fill in certificates according to <http://hasseg.org/blog/post/161/gmail-backups-with-fetchmail-on-os-x/>
2. Mails are stored in `/var/mail/[your name]` instead of `/var/spool/mail/[your name]`

However, we don't have a `/etc/ssl/certs/ca-certificates.crt` for `msmtp` so we cannot send emails now. I change it to `/Users/[username]/ssl/certs/gmailpop.pem` and it works fine. 

Now we can receive and send emails. But `mutt` says the mailbox is read only. The permission of `/var/mail/xjia` is fine but `/var/mail` is not writable. So 

    sudo chmod o+w /var/mail

solves the problem.

