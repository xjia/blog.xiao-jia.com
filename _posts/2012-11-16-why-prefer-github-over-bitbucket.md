title: Why prefer GitHub over Bitbucket
tag: git, github, bitbucket
time: 2012-11-16 23:59:14

- 项目太少，没什么可看的
- <del>没有 inline comments</del>
- 很大的 full commit 浏览的时候性能很差
- 没有 gist
- 没有 per-project web hosting ，blog.xiao-jia.com 这种就不行
- <del>wiki 编辑器不是等宽字体</del> [现在可以了](https://bitbucket.org/site/master/issue/5519/use-a-fixed-width-font-for-the-wiki-text)
- 不能在线编辑 source 并 commit

但是 Bitbucket 也有 3 个好处

- 占有了 xjia 这个用户名
- 有无限私有空间
- project 里有界面控制 Username aliases ，可以修正不一致的 commit author

如果 Bitbucket 把周边的功能再做完善一些的话，或许会考虑迁移到上面去。

