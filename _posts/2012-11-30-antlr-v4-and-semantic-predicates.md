title: ANTLR v4 and Semantic Predicates
tag: antlr, compiler, 工程
time: 2012-11-30 18:00:00

这两天把 ANTLR 4 网上的文档看完了，下午试验了一下，记录在这里。

目前 ANTLR 4 还是 beta 状态，可以到 http://antlr4.org/download/antlr-4.0b3-complete.jar 下载，我重命名后放到了 `~/antlr/antlr4-complete.jar`，方便以后用新版本覆盖。然后在 `.bash_profile` 里加了几句：

    export CLASSPATH=".:/home/xjia/antlr/antlr4-complete.jar:$CLASSPATH"
    alias antlr4='java -jar /home/xjia/antlr/antlr4-complete.jar'
    alias grun='java org.antlr.v4.runtime.misc.TestRig'

因为对 semantic predicates 的使用不是很清楚，所以上网找了一下，觉得 StackOverflow 上的[这个帖子](http://stackoverflow.com/a/3056517/940313)非常好，就试了一下。

首先建立一个新的文件，叫 `Numbers.g4`，内容如下：

    grammar Numbers;

    parse
      : atom (',' atom)* EOF
      ;

    atom
      : low  { System.out.println("low  = " + $low.text); }
      | high { System.out.println("high = " + $high.text); }
      ;

    low
      : { Integer.valueOf(getCurrentToken().getText()) <= 500 }? Number
      ;

    high
      : Number
      ;

    Number
      : Digit Digit Digit
      | Digit Digit
      | Digit
      ;

    fragment Digit
      : '0'..'9'
      ;

    WhiteSpace
      : (' ' | '\\t' | '\\r' | '\\n') { skip(); }
      ;

然后就可以试试看了：

    $ antlr4 Numbers.g4
    $ javac *.java
    $ grun Numbers parse
    123, 999, 456, 700, 89, 0
    ^D
    low  = 123
    high = 999
    low  = 456
    high = 700
    low  = 89
    low  = 0

这个 `grun` 工具非常方便，可以快速验证 parser 写没写对，但是注意要先把 ANTLR 生成的 Java 文件编译，才能用 `grun`，不然会出现：

    Exception in thread "main" java.lang.ClassNotFoundException: Numbers
            at java.net.URLClassLoader$1.run(URLClassLoader.java:366)
            at java.net.URLClassLoader$1.run(URLClassLoader.java:355)
            at java.security.AccessController.doPrivileged(Native Method)
            at java.net.URLClassLoader.findClass(URLClassLoader.java:354)
            at java.lang.ClassLoader.loadClass(ClassLoader.java:423)
            at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:308)
            at java.lang.ClassLoader.loadClass(ClassLoader.java:356)
            at org.antlr.v4.runtime.misc.TestRig.main(TestRig.java:148)
